$.ajax({
    url: 'endpoints/getVehicle.php',
    type: 'GET',
    dataType: 'json',
    success: function (res) {
        let responseHtml = "";

        res.forEach(function (vehicle) {
            responseHtml += createCarView(vehicle);
        })

        document.getElementById("carsContainer").innerHTML = responseHtml;
    }
});

$(document).on("click", ".carForSale", function () {
    var vehicleId = $(this).data('id');

    $.ajax({
        url: 'endpoints/getVehicleById',
        type: 'GET',
        data: {vehicleId: vehicleId},
        success: function (res) {
            let response = "";
            res = JSON.parse(res);
            res.attributes.forEach(function (attributeObject) {
                response += "<div style='border-bottom: 1px solid #e9e1e1; margin-bottom: 8px'>" +
                    "<span class='attributeName'>" + snakeCaseToCamelCase(attributeObject.attributeName) + "</span>" +
                    "<span>" + attributeObject.attributeValue + "</span>" +
                    "</div>";
            })


            document.getElementById("carModalDetails").innerHTML = response;
        }
    });
});

function snakeCaseToCamelCase(param) {
    let param2 = param.split("_").join(" ");
    param2[0] = param.charAt(0).toUpperCase();

    return param2;
}
