document.getElementById("index").className = "";
document.getElementById("cars").className = "active";
document.getElementById("dealer").className = "";

let carAttributes = [];
let attributeValues = [];

$.ajax({
    url: 'endpoints/getAttributes.php',
    type: 'GET',
    dataType: 'json',
    success: function (res) {
        let attributeHtmlElement = document.getElementById("attribute");

        res.forEach(function (attribute) {
            let optionElement = document.createElement("option");
            let attributeName = attribute.name.split("_").join(" ");
            attributeName[0] = attributeName.charAt(0).toUpperCase();
            optionElement.innerText = attributeName;
            optionElement.setAttribute("data-id", attribute.id);
            attributeHtmlElement.appendChild(optionElement);
        });

        carAttributes = res;
    }
});

function sendRequestToBackend() {
    let attributeHtmlElement = document.getElementById("attribute");
    let selectedIndex = attributeHtmlElement.selectedIndex;
    let selectedFilterValue = document.getElementById("attributeValues").selectedIndex;

    $.ajax({
        url: 'endpoints/filterCars',
        data: {
            attribute: carAttributes[selectedIndex - 1],
            selectedValue: attributeValues[selectedFilterValue - 1]
        },
        type: 'POST',
        success: function (response) {
            document.getElementById("carsContainer").innerHTML = "";
            let responseHTML = "";

            response = JSON.parse(response);

            response.forEach(function (car) {
                responseHTML += createCarView(car);
            })

            document.getElementById("carsContainer").innerHTML = responseHTML;
        }
    });
}

function onSelectedAttributeChanged() {

    let attributeHtmlElement = document.getElementById("attribute");
    let selectedIndex = attributeHtmlElement.selectedIndex;
    let attributeId = carAttributes[selectedIndex - 1].id;

    loadAttributeValuesById(attributeId);
}

function loadAttributeValuesById(attributeId) {

    $.ajax({
        url: 'endpoints/getAttributesValues.php',
        type: 'GET',
        dataType: 'json',
        data: {
            attributeId: attributeId
        },
        success: function (res) {
            attributeValues = res;
            populateAttributeValuesDropdown(res);
        }
    });
}

function populateAttributeValuesDropdown(attributeValues) {
    let selectedAttHtmlElement = document.getElementById("attributeValues");
    let length = selectedAttHtmlElement.options.length;

    for (let i = length - 1; i >= 1; i--) {
        selectedAttHtmlElement.options[i] = null;
    }
    attributeValues.forEach(attributeValue => {
        let optionElement = document.createElement("option");
        optionElement.innerText = attributeValue;
        optionElement.setAttribute("data-id", attributeValue.id);
        selectedAttHtmlElement.appendChild(optionElement);
    });
}

function createCarView(car) {

    let image = car.image ? car.image : "no-image.png";

    return "  <div class=\"col-lg-3 col-sm-6 col-xl-12 carForSale\" style=\" height: 500px margin: 16px;\" data-id='"+ car.id+"' data-toggle=\"modal\" data-target=\"#myModal\">" +
        "            <div id=\"pic\" class=\"background-image-1\" style=\"background-image: url('images/" + image + "')\" >" +
        "        </div>" +
        "            <div style=\" background-color: #e5eeee; border-bottom-left-radius: 8px; border-bottom-right-radius: 8px\">" +
        "                <span style=\"display:block; font-size: 25px; padding: 8px; color: #0b2e13\">"+car.manufacturer + " " +car.model+"</span>" +
        "                <span style=\"display:block; font-size: 25px; margin: 8px \">"+car.price+"&euro;</span>" +
        "            </div>" +
        "        </div>";
}