function formValidation() {

    document.getElementById("registration").addEventListener("submit", function (event) {
        event.preventDefault();
        let name = document.getElementById("name");
        let lastName = document.getElementById("lastName");
        let email = document.getElementById("email");
        let phone = document.getElementById("phone");
        let password = document.getElementById("password");
        let mailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        let phoneFormat = /^\d{10}$/;
        let returnValue = true;

        if (!name.value) {
            document.getElementById("nameWarning").innerText = "*Name is mandatory!";
            returnValue = returnValue && false;
        }
        else {
            document.getElementById("nameWarning").innerText = "";
            returnValue = returnValue && true;
        }

        if (!lastName.value) {
            document.getElementById("lastNameWarning").innerText = "*Last name is mandatory!";
            returnValue = returnValue &&false;
        } else {
            document.getElementById("lastNameWarning").innerText = "";
            returnValue = returnValue && true;
        }

        if (email == null || !email.value.match(mailFormat)) {
            document.getElementById("emailWarning").innerText = "*Please enter valid email address!";
            returnValue = returnValue && false;
        } else {
            document.getElementById("emailWarning").innerText = "";
            returnValue = returnValue && true;
        }

        if (phone == null || !phone.value.match(phoneFormat)) {
            document.getElementById("phoneWarning").innerText = "*Please enter valid phone number!";
            returnValue = returnValue && false;
        } else {
            document.getElementById("phoneWarning").innerText = "";
            returnValue = returnValue &&true;
        }

        if (!password.value) {
            document.getElementById("passwordWarning").innerText = "*Password is mandatory!";
            returnValue = returnValue && false;
        } else {
            document.getElementById("passwordWarning").innerText = "";
            returnValue = returnValue && true;
        }

        if(returnValue === true) {

            $(document).ready(function () {
            let obj = {
                nameInput : name.value,
                lastNameInput : lastName.value,
                emailInput : email.value,
                phoneInput : phone.value,
                passwordInput : password.value
            };

            $.ajax({
                type: "POST",
                url : "../validation/userValidation.php",
                data: {
                    json: JSON.stringify(obj)
                },
                success: function (response) {
                    alert("Your Application is received :)");

                    window.setTimeout(function(){

                        // Move to a new location or you can do something else
                        window.location.href = "../cars.php";

                    }, 2000);
                },
                error(errorObj) {
                    JSON.stringify(errorObj);
                    let errorArray = JSON.parse(errorObj.responseText);

                    errorArray.forEach(function (error) {
                        alert(error);
                    })
                }
            })
        })
        }




    })
}