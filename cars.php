<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Cars</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="style.css">
    <script type="text/javascript" src="scripts/vehicleLoading.js"></script>
    <style>
        .attributeName {
            width: 150px; display: inline-block; margin-inline-end: 20px
        }
        .background-image-1
        {
            background-size: contain;
            background-repeat: no-repeat;
            background-color: gray;
            background-position: 50% 50%;
            height: 450px;
            padding: 0;
            overflow: hidden;
            top:-80px;
            z-index: 0;
        }
    </style>
</head>
<body>

<div>
    <?php
    require_once "nav-bar.html";
    ?>
</div>

<button type="button" class="collapsible activated">Filters</button>
<div class="filters">

    <select id="attribute" onchange="onSelectedAttributeChanged()">
        <option selected disabled>Select a attribute</option>
    </select>

    <button onclick="sendRequestToBackend()">Send it</button>
    <br>
    <select id="attributeValues">
        <option selected disabled>Select a value</option>
    </select>

</div>

<div id="carsForSale">
    <div class="row" style="padding: 8px" id="carsContainer">

    </div>
</div>

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Informacije o automobilu</h4>
            </div>
            <div class="modal-body" id="carModalDetails">
                <p>Some text in the modal.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="scripts/filterLoading.js"></script>
<script type="text/javascript" src="scripts/collapseDiv.js"></script>

</body>
</html>



