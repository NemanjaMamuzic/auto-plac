<?php

require_once "../db/db_config.php";
require_once "MappingUtils.php";

$selectAllQuery = "SELECT v.*, vi.vehicle_image AS image, m.model, ma.manufacturer FROM vehicle v 
LEFT JOIN vehicle_image vi ON v.id_vehicle = vi.vehicle_id
JOIN model m ON v.model_id = m.id_model
JOIN manufacturer ma ON m.manufacturer_id = ma.id_manufacturer;

";
$quaryResults = mysqli_query($connection, $selectAllQuery);
$mappedResults = [];

while ($vehicle = mysqli_fetch_array($quaryResults, MYSQLI_ASSOC)) {
    $car = mapCarToResponse($vehicle);
    $car['manufacturer'] = $vehicle['manufacturer'];
    $car['model'] = $vehicle['model'];

    array_push($mappedResults, $car);
}

echo json_encode($mappedResults);