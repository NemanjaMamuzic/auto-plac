<?php

require_once "../db/db_config.php";

$selectAllQuery = "SELECT * FROM vehicle_image";
$queryResults = mysqli_query($connection,$selectAllQuery);
$mappedResults = [];

while($image = mysqli_fetch_array($queryResults, MYSQLI_ASSOC)){
    $mappedImage["id"] = $image["id_vehicle_image"];
    $mappedImage["image"] = $image["vehicle_image"];
    $mappedImage["vehicle_id"] = $image["vehicle_id"];
    array_push($mappedResults, $mappedImage);
};

echo json_encode($mappedResults);