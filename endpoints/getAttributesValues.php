<?php

require_once "../db/db_config.php";
$attributeId = $_GET['attributeId'];

$sql = "SELECT DISTINCT attribute_value FROM vehicle_attribute WHERE attribute_id = " . $attributeId;

$queryResults = mysqli_query($connection, $sql);
$attributeValues = array();

while ($row = mysqli_fetch_array($queryResults)) {
    $attvalue = $row['attribute_value'];
    array_push($attributeValues, $attvalue);
};

echo json_encode($attributeValues);