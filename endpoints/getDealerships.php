<?php

require_once "../db/db_config.php";

$selectAllQuery = "SELECT * FROM dealership";
$queryResults = mysqli_query($connection, $selectAllQuery);
$mappedResults = [];

while ($dealership = mysqli_fetch_array($queryResults, MYSQLI_ASSOC)) {
    $mappedDealership["id"] = $dealership["id_dealership"];
    $mappedDealership["city"] = $dealership["city"];
    $mappedDealership["street"] = $dealership["street"];
    $mappedDealership["contact_number"] = $dealership["contact_number"];
    $mappedDealership["work_time"] = $dealership["work_time"];
    $mappedDealership["latitude"] = $dealership["latitude"];
    $mappedDealership["longitude"] = $dealership["longitude"];
    array_push($mappedResults, $mappedDealership);
};

echo json_encode($mappedResults);


