<?php

function mapCarToResponse($car)
{
    $mappedVehicle["id"] = $car["id_vehicle"];
    $mappedVehicle["price"] = $car["price"];
    $mappedVehicle["owner_id"] = $car["owner_id"];
    $mappedVehicle["model_id"] = $car["model_id"];
    $mappedVehicle["dealership_id"] = $car["dealership_id"];
    $mappedVehicle["image"] = $car["image"];

    return $mappedVehicle;
}