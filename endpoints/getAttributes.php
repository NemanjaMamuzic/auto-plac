<?php

require_once "../db/db_config.php";

$selectAllQuery = "SELECT * FROM attribute";
$queryResults = mysqli_query($connection, $selectAllQuery);
$mapedResults = [];

while($attribute = mysqli_fetch_array($queryResults, MYSQLI_ASSOC)) {
    $mappedAttribute["id"] = $attribute["id_attribute"];
    $mappedAttribute["name"] = $attribute["attribute_name"];
    array_push($mapedResults, $mappedAttribute);
};

echo json_encode($mapedResults);



