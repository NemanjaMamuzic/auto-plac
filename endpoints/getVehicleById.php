<?php

require_once "../db/db_config.php";
require_once "MappingUtils.php";

$id = $_GET['vehicleId'];

$selectAllQuery = "SELECT v.*, vi.vehicle_image AS image 
                   FROM vehicle v 
                   LEFT JOIN vehicle_image vi ON v.id_vehicle = vi.vehicle_id
                   LEFT JOIN vehicle_attribute va ON v.id_vehicle = va.vehicle_id
                   WHERE v.id_vehicle = $id";

$queryResults = mysqli_query($connection, $selectAllQuery) or die(mysqli_error($connection));

$vehicle = mysqli_fetch_array($queryResults, MYSQLI_ASSOC);

$response = mapCarToResponse($vehicle);

$selectVehicleAttributes = "SELECT v.*, a.attribute_name FROM vehicle_attribute v JOIN attribute a ON v.attribute_id = a.id_attribute WHERE v.vehicle_id = $id";
$vehicleAttributeResults = mysqli_query($connection, $selectVehicleAttributes) or die(mysqli_error($connection));

$attributes = [];

while ($attribute = mysqli_fetch_array($vehicleAttributeResults, MYSQLI_ASSOC)) {
    $attributeResponse['attributeName'] = $attribute['attribute_name'];
    $attributeResponse['attributeValue'] = $attribute['attribute_value'];

    array_push($attributes, $attributeResponse);
}

$response['attributes'] = $attributes;

echo json_encode($response);