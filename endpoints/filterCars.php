<?php
require_once "../db/db_config.php";
require_once "MappingUtils.php";

$attributeId = $_POST['attribute']['id'];
$filterValue = $_POST['selectedValue'];

$filterQuery = "SELECT v.*, vi.vehicle_image  AS image, m.model, ma.manufacturer  FROM vehicle v 
                LEFT JOIN vehicle_attribute va ON v.id_vehicle = va.vehicle_id 
                LEFT JOIN vehicle_image vi on va.vehicle_id = vi.vehicle_id            
                JOIN model m ON v.model_id = m.id_model
                JOIN manufacturer ma ON m.manufacturer_id = ma.id_manufacturer
                WHERE va.attribute_id = $attributeId AND va.attribute_value = '" . $filterValue . "'";

$result = mysqli_query($connection, $filterQuery) or die (mysqli_error($connection));

$response = [];

while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    $car = mapCarToResponse($row);
    $car['model'] = $row['model'];
    $car['manufacturer'] = $row['manufacturer'];

    array_push($response, $car);
}

echo json_encode($response);
