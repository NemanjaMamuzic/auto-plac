<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Dealerships</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="style.css">

</head>
<body>

<?php
require_once "nav-bar.html";
?>

<script type="text/javascript">
    document.getElementById("index").className = "";
    document.getElementById("cars").className  = "";
    document.getElementById("dealer").className  = "active";
</script>

<div id="dealership">
    <div id="map">
        <button onclick="initMap()">Find Me</button>
    </div>
    <h1 id="location"></h1>
    <p id="infodealer"></p>
    <p id="lat"></p>
    <p id="lon"></p>
</div>

<script>
    $.ajax({
        url: 'endpoints/getDealerships.php',
        type: 'GET',
        dataType: 'json',
        success: function (res) {
            let indexOfCity = document.getElementById("location");
            let infoDealer = document.getElementById("infodealer");
            let latitude = document.getElementById("lat");
            let longitude = document.getElementById("lon");
            res.forEach(function (dealership) {
                indexOfCity.innerText = dealership.id + ". " + dealership.city;
                infoDealer.innerText = "Street:" + dealership.street + "\n" + "Contact Number:" + dealership.contact_number + "\n" + "Work Time:" + dealership.work_time;
                latitude.innerText =  dealership.latitude;
                longitude.innerText = dealership.longitude;
            })

        }
    });

    function initMap() {
        var subotica = {lat: 46.119514, lng: 19.681601};
        var map = new google.maps.Map(
            document.getElementById('map'), {zoom: 14, center: subotica});
        var marker = new google.maps.Marker({position: subotica, map: map});

    }
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAExoAXdlOX-88JHYdoKzSg9lRUiyBUf6c&callback=initMap">
</script>
</body>
</html>