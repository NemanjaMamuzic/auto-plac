<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Home</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="style.css">
</head>

<body class="indexBody">
    <div class="text"> <span> Welcome to the official website of our dealership! We offer exquisite, personalised, service. <br> At any time of the day, pick out the second-hand vehicle that suits you from our vast selection.</span></div>

<?php
    require_once "nav-bar.html";
?>

<script type="text/javascript">
    document.getElementById("index").className = "active";
    document.getElementById("cars").className  = "";
    document.getElementById("dealer").className  = "";
</script>

</body>
</html>



