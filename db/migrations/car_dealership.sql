-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 20, 2020 at 04:36 PM
-- Server version: 5.7.23
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `car_dealership`
--
CREATE DATABASE IF NOT EXISTS `car_dealership` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `car_dealership`;

-- --------------------------------------------------------

--
-- Table structure for table `attribute`
--

DROP TABLE IF EXISTS `attribute`;
CREATE TABLE IF NOT EXISTS `attribute` (
  `id_attribute` int(11) NOT NULL,
  `attribute_name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_attribute`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attribute`
--

INSERT INTO `attribute` (`id_attribute`, `attribute_name`) VALUES
(1, 'year_of_production'),
(2, 'fuel_type'),
(3, 'transmission');

-- --------------------------------------------------------

--
-- Table structure for table `dealership`
--

DROP TABLE IF EXISTS `dealership`;
CREATE TABLE IF NOT EXISTS `dealership` (
  `id_dealership` int(11) NOT NULL,
  `city` varchar(30) DEFAULT NULL,
  `street` varchar(30) DEFAULT NULL,
  `contact_number` varchar(20) DEFAULT NULL,
  `work_time` varchar(20) DEFAULT NULL,
  `latitude` decimal(8,6) DEFAULT NULL,
  `longitude` decimal(8,6) DEFAULT NULL,
  PRIMARY KEY (`id_dealership`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dealership`
--

INSERT INTO `dealership` (`id_dealership`, `city`, `street`, `contact_number`, `work_time`, `latitude`, `longitude`) VALUES
(1, 'Subotica', '51. Divizije', '024 550 255', '08:00-16:00', '46.119514', '19.681601');

-- --------------------------------------------------------

--
-- Table structure for table `employe`
--

DROP TABLE IF EXISTS `employe`;
CREATE TABLE IF NOT EXISTS `employe` (
  `id_employe` int(11) NOT NULL,
  `name_employe` varchar(20) DEFAULT NULL,
  `last_name` varchar(20) DEFAULT NULL,
  `contact_number` varchar(20) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `dealership_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_employe`),
  KEY `FK_dealership_employe` (`dealership_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employe`
--

INSERT INTO `employe` (`id_employe`, `name_employe`, `last_name`, `contact_number`, `email`, `dealership_id`) VALUES
(1, 'Mirko', 'Mirkovic', '064 45 88 659', 'mirkomirkovic@gmail.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `manufacturer`
--

DROP TABLE IF EXISTS `manufacturer`;
CREATE TABLE IF NOT EXISTS `manufacturer` (
  `id_manufacturer` int(11) NOT NULL,
  `manufacturer` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_manufacturer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `manufacturer`
--

INSERT INTO `manufacturer` (`id_manufacturer`, `manufacturer`) VALUES
(1, 'Citroen');

-- --------------------------------------------------------

--
-- Table structure for table `model`
--

DROP TABLE IF EXISTS `model`;
CREATE TABLE IF NOT EXISTS `model` (
  `id_model` int(11) NOT NULL,
  `model` varchar(30) DEFAULT NULL,
  `manufacturer_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_model`),
  KEY `FK_manufacturer` (`manufacturer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `model`
--

INSERT INTO `model` (`id_model`, `model`, `manufacturer_id`) VALUES
(1, 'C2', 1);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle`
--

DROP TABLE IF EXISTS `vehicle`;
CREATE TABLE IF NOT EXISTS `vehicle` (
  `id_vehicle` int(11) NOT NULL,
  `price` decimal(10,0) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `model_id` int(11) DEFAULT NULL,
  `dealership_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_vehicle`),
  KEY `FK_owner` (`owner_id`),
  KEY `FK_model` (`model_id`),
  KEY `FK_dealership` (`dealership_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle`
--

INSERT INTO `vehicle` (`id_vehicle`, `price`, `owner_id`, `model_id`, `dealership_id`) VALUES
(1, '2000', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_attribute`
--

DROP TABLE IF EXISTS `vehicle_attribute`;
CREATE TABLE IF NOT EXISTS `vehicle_attribute` (
  `id_vehicle_attribute` int(11) NOT NULL,
  `attribute_value` varchar(30) DEFAULT NULL,
  `vehicle_id` int(11) DEFAULT NULL,
  `attribute_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_vehicle_attribute`),
  KEY `FK_vehicle` (`vehicle_id`),
  KEY `FK_attribute` (`attribute_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle_attribute`
--

INSERT INTO `vehicle_attribute` (`id_vehicle_attribute`, `attribute_value`, `vehicle_id`, `attribute_id`) VALUES
(1, '2001', 1, 1),
(2, 'Petrol', 1, 2),
(3, 'Manual 5-gears', 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_image`
--

DROP TABLE IF EXISTS `vehicle_image`;
CREATE TABLE IF NOT EXISTS `vehicle_image` (
  `id_vehicle_image` int(11) NOT NULL,
  `vehicle_image` varchar(45) DEFAULT NULL,
  `vehicle_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_vehicle_image`),
  KEY `FK_vehicle_image` (`vehicle_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle_image`
--

INSERT INTO `vehicle_image` (`id_vehicle_image`, `vehicle_image`, `vehicle_id`) VALUES
(1, 'edjgihfdlgkmdfngokl.JPG', 1);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_owner`
--

DROP TABLE IF EXISTS `vehicle_owner`;
CREATE TABLE IF NOT EXISTS `vehicle_owner` (
  `id_owner` int(11) NOT NULL,
  `contact_number` varchar(20) DEFAULT NULL,
  `name_owner` varchar(20) DEFAULT NULL,
  `last_name_owner` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_owner`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle_owner`
--

INSERT INTO `vehicle_owner` (`id_owner`, `contact_number`, `name_owner`, `last_name_owner`) VALUES
(1, '061 89 25 1698', 'Pera', 'Perovic');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
