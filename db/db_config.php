<?php

define("HOST", "localhost");
define("USERNAME", "root");
define("PASSWORD", "");
define("DATABASE", "car_dealership");

$connection = mysqli_connect(HOST, USERNAME, PASSWORD, DATABASE) or die(mysqli_error($connection));
